const { Client } = require("pg");
require("dotenv").config();

class Admin {
  #email = "john@mail.com";
  #password = "12345";

  constructor(userObj) {
    this.client = new Client({
      user: "postgres",
      host: "localhost",
      password: "134206ok",
      // database: "event",
      port: 5432,
    });

    this.authenticated = this.#authenticate(userObj);
  }

  #authenticate = (userObj) => {
    return this.#email === userObj.email && this.#password === userObj.password;
  };

  createDatabase = async (databaseName) => {
    const queryCreateDatabase = `CREATE DATABASE ${databaseName};`;
    if (this.authenticated) {
      try {
        await this.client.connect(); // gets connection
        await this.client.query(queryCreateDatabase); // sends queries
        return console.log("Create Database Suceed");
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }
    return console.log("Anda tidak memiliki hak akses");
  };

  createTable = async () => {
    const queryCreateTable = `CREATE TABLE "customers"(
        customer_id SERIAL NOT NULL PRIMARY KEY,
        name VARCHAR(50) NOT NULL,
        age INT NOT NULL,
        phone_number VARCHAR(50) NOT NULL
      );
      
      CREATE TABLE "event"(
        event_id SERIAL NOT NULL PRIMARY KEY,
        event_name VARCHAR(50) NOT NULL,
        event_date DATE NOT NULL DEFAULT CURRENT_DATE
      );
      
      CREATE TABLE "ticket"(
        ticket_id SERIAL NOT NULL PRIMARY KEY,
        ticket_name VARCHAR(50) NOT NULL UNIQUE,
        price INT NOT NULL,
        customer_id INT NULL REFERENCES customers(customer_id),
        event_id INT NULL REFERENCES event(event_id)
      );`;

    if (this.authenticated) {
      // gets connection
      try {
        // gets connection
        await this.client.connect();
        await this.client.query(queryCreateTable); // sends queries
        return console.log("Create Table Suceed");
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }

    return console.log("Anda tidak memiliki hak akses");
  };

  insertValues = async () => {
    const queryInsert = `INSERT INTO customers(name,age,phone_number) VALUES('Bambang',21,'089627182');
    INSERT INTO EVENT(event_name) VALUES('Konser Musik');
    INSERT INTO TICKET(ticket_name,price,customer_id,event_id) VALUES('A02',30000,(select customer_id from customers where customer_id = 1),(select event_id from event where event_id = 1));`;

    if (this.authenticated) {
      // gets connection
      try {
        // gets connection
        await this.client.connect();
        await this.client.query(queryInsert); // sends queries
        return console.log("Insert Value Suceed");
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }
  };

  readValues = async () => {
    const queryRead = `SELECT customers.customer_id,
                       name,
                       age,
                       phone_number,
                       ticket_name 
                       FROM 
                        customers 
                       INNER JOIN ticket 
                        ON ticket.customer_id = customers.customer_id;`;

    if (this.authenticated) {
      // gets connection
      try {
        // gets connection
        await this.client.connect();
        let result = await this.client.query(queryRead); // sends queries
        return console.log(result.rows);
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }
  };

  updateValues = async (
    table_name,
    table_attribute,
    attribute_id,
    value1,
    value2
  ) => {
    const queryUpdate = `UPDATE ${table_name} SET ${table_attribute} = $1 WHERE ${attribute_id} = $2;`;

    if (this.authenticated) {
      // gets connection
      try {
        // gets connection
        await this.client.connect();
        await this.client.query(queryUpdate, [value1, value2]); // sends queries
        return console.log("Berhasil Mengubah Value");
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }
  };

  deleteValues = async (table_name, table_id, value1) => {
    const queryDelete = `DELETE FROM ${table_name} WHERE ${table_id} = $1;`;

    if (this.authenticated) {
      // gets connection
      try {
        // gets connection
        await this.client.connect();
        await this.client.query(queryDelete, [value1]); // sends queries
        return console.log("Delete Value Suceed");
      } catch (error) {
        console.error(error.stack);
        return false;
      } finally {
        await this.client.end(); // closes connection
      }
    }
  };
}

// Instantia si class MyRdbms
const adminPertama = new Admin({ email: "john@mail.com", password: "12345" });

//--------1 Create Database----------
adminPertama.createDatabase("Event");
//--------2 Create Table(masukin manual Database)---------
// adminPertama.createTable();
// -------3 Insert Values-------------
// adminPertama.insertValues();
// -------4 Read Values---------
// adminPertama.readValues();
// -------5 Update Value--------
// Update Table Ticket
// adminPertama.updateValues("ticket", "ticket_name", "ticket_id", "A21", "1");
//--------6 Delete Value--------
// adminPertama.deleteValues("ticket", "ticket_id", "1");
